# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Rainbow pen / Dark                                  | 1~5%     | Y        |


---

### How to use 

    按旁邊的小圖就可以使用各種功能囉~

### Function description

    //------------------------------------------
	*ranbow:
	可以將筆跡改成彩色的，會隨著畫的時候改變顏色
	而且有模擬真實筆觸(調很久!!!!!!!!!!!!)

	israinbow_Pencil(e):
	可以在canvas on mouse down 的時候設定彩色筆跡
	開始畫圖的設定

	rainbow_PencilDraw(e):
	可以在canvas on mouse move 的時候設定彩色筆跡
	以及畫出來

	*反色:
	將畫板反色
	
	DARK():
	只管呼叫就行了



	
### Gitlab page link

    https://106062272.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    彩虹筆寫很久可以加多一點分ㄇQQQQQ

<style>
table th{
    width: 100%;
}
</style>